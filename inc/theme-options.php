<?php
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page([
        'page_title' => __('Thema instellingen'),
        'menu_title' => __('Thema instellingen')
    ]);
}

add_filter( 'nav_menu_link_attributes', function ($atts, $item) {
    $item_has_children = in_array('menu-item-has-children', $item->classes);
    if ($item_has_children) {
        $atts['aria-expanded'] = 'false';
    }
    return $atts;
}, 10, 3);
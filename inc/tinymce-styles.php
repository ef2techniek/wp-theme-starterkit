<?php
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', function ($buttons) {
    array_unshift($buttons, 'styleselect');

    return $buttons;
});

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', function ($init_array) {
    $style_formats = [
        [
            'title'      => 'Primaire button',
            'selector'   => 'a',
            'attributes' => ['class' => 'btn--primary']
        ],
        [
            'title'      => 'Secundaire button',
            'selector'   => 'a',
            'attributes' => ['class' => 'btn--secondary']
        ],
        [
            'title'      => 'Outline button',
            'selector'   => 'a',
            'attributes' => ['class' => 'btn--outline']
        ],
        [
            'title'      => 'Textlink',
            'selector'   => 'a',
            'attributes' => ['class' => 'text-link']
        ],
        [
            'title'      => 'Intro tekst',
            'selector'   => 'p',
            'attributes' => ['class' => 'intro-text']
        ],
        [
            'title'      => 'Quote',
            'selector'   => 'p',
            'attributes' => ['class' => 'quote']
        ],
        [
            'title'      => 'Label',
            'selector'   => 'p',
            'attributes' => ['class' => 'label']
        ],
    ];

    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
});

add_action('init', function () {
    add_editor_style('editor.css');
});
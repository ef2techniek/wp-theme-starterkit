# WordPress EF2 Starter Thema #

### Hoe te gebruiken?

Je checked dit thema uit in de map wp-content/themes/ dan verwijder je in de map ef2_custom de map .git. Omdat dit thema nu onderdeel is van jouw WordPress project.
De gecompilede bestanden zitten niet in de git, zodra je het thema uitgechecked hebt, draai je `npm install` en daarna een `npm run prod/dev`

### File templates:

**front-page.php**

Dit template word gebruikt om de ingestelde "homepagine" ofwel front-page weer te geven.

**page.php**

Dit template word gebruikt om het standaard pagina template weer te geven.
Ga je gebruik maken van verschillende pagina templates maak dan een **page-templatename.php** aan met bovenin het volgende:

````
/*
* Template Name: Naam van pagina template
*/
````

**archive.php**

Dit template word gebruikt om de categorieén pagina's weer te geven.

**404.php**

Dit template word gebruikt om de 404 pagina weer te geven

**header.php**

Dit template word gebruikt om de header weer te geven wil je verschillende headers gebruiken maak dan een bestand aan **header-naamvanheader.php**
en gebruik het volgende om hem weer te geven:

````
get_header('naamvanheader');
````

**footer.php**

Dit template word gebruikt om de footer weer te geven wil je verschillende headers gebruiken maak dan een bestand aan **footer-naamvanfooter.php**
en gebruik het volgende om hem weer te geven:

````
get_footer('naamvanfooter');
````

**single.php**

Dit template word gebruikt om de post detail pagina's weer te geven. Dus standaard de blog berichten van wordpress. Heb je een eigen post type aangemaakt dan kan je hier ook een eigen single pagina voor maken.
Dit doe je door een **single-slug.php** aan te maken en slug vervang je door de slug van jouw custom post type. 
const mainMenu = document.getElementById('site-navigation');
const menuToggle = document.getElementById('menu-toggle');

if (!_.isNil(mainMenu)) {
    const submenus = mainMenu.getElementsByClassName('menu-item-has-children');

    // Close all submenus when clicking outside submenu
    if (!_.isEmpty(submenus)) {
        document.onclick = function (e) {
            if (!e.target.classList.contains('menu') && !e.target.classList.contains('menu-item-has-children-toggle')) {
                closeAllSubmenus();
            }
        }
    }

    // Submenu open and close
    Array.from(submenus).forEach(submenu => {
        let submenuButton = submenu.querySelector('a');

        submenuButton.classList.add('menu-item-has-children-toggle');

        submenuButton.addEventListener('click', (e) => {

            if (submenuButton.getAttribute('aria-expanded') !== 'true') {
                closeAllSubmenus();

                submenu.classList.add('open');
                submenuButton.setAttribute('aria-expanded', 'true');

                return;
            }

            submenu.classList.remove('open');
            submenuButton.setAttribute('aria-expanded', 'false');
        });
    });

    function closeAllSubmenus() {
        Array.from(submenus).forEach(submenu => {
            submenu.querySelector('a').setAttribute('aria-expanded', 'false');

            submenu.classList.remove('open');
        });
    }

    // Hamburger menu toggle
    if (!_.isNil(menuToggle)) {
        menuToggle.addEventListener('click',  () => {
            if (menuToggle.getAttribute('aria-expanded') === 'true') {
                menuToggle.setAttribute('aria-expanded', 'false');
                document.body.classList.remove('main-menu-active');
                mainMenu.classList.remove('main-menu-open');
                return;
            }

            menuToggle.setAttribute('aria-expanded', 'true');
            document.body.classList.add('main-menu-active')
            mainMenu.classList.add('main-menu-open');
        });

        document.addEventListener('keydown', (e) => {
            if (e.key !== 'Escape') {
                return;
            }

            menuToggle.setAttribute('aria-expanded', 'false');
            document.body.classList.remove('main-menu-active');
            mainMenu.classList.remove('main-menu-open');
        });
    }

    // Hamburger menu accessibility
    const mainMenuList = mainMenu.querySelector('ul.menu');

    if (!_.isNil(mainMenuList)) {
        const lastMenuItem = mainMenuList.lastChild.firstChild;
        accessibleHamburger(lastMenuItem, menuToggle);
    }

    function accessibleHamburger(lastMenuItem, menuToggle) {
        if (window.innerWidth > 1200) {
            return;
        }

        if (!_.isNil(lastMenuItem)) {
            lastMenuItem.addEventListener('keydown', (event) => {
                if (event.shiftKey) {
                    return;
                }

                event.preventDefault();
                menuToggle.focus();
            });
        }

        if (!_.isNil(menuToggle)) {
            menuToggle.addEventListener('keydown', (event) => {
                if (menuToggle.getAttribute('aria-expanded') !== 'true' || !event.shiftKey) {
                    return;
                }

                event.preventDefault();
                lastMenuItem.focus();
            });
        }
    }
}
// Remove no-js class from body when Javascript is activated
document.body.classList.remove('no-js');
document.body.classList.add('js')

// Open menu on mobile with menu-toggle
document.addEventListener('click', () => {
    if (document.getElementById('site-navigation').classList.contains('toggled')) {
        document.body.classList.add('menu-open');
    } else {
        document.body.classList.remove('menu-open');
    }
});

document.addEventListener('keydown', (e) => {
    if(e.key === 'Escape') {
        document.body.classList.remove('menu-open');
        document.getElementById('site-navigation').classList.remove('toggled')
        document.getElementById('menu-toggle').setAttribute('aria-expanded', 'false');
    }
});

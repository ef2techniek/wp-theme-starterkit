let mix = require('laravel-mix');

require('laravel-mix-polyfill');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({ processCssUrls: false });

mix.js('js/source/theme/*', 'js/bundle.js')
    .js('js/source/vue/app.js', 'js/app.js').vue({ version: 2 })
    .sass('scss/style.scss', '')
    .sass('scss/editor.scss', '')
    .sourceMaps(true, 'source-map')
    .polyfill({
        enabled: true,
        useBuiltIns: "entry",
        targets: {"ie": 11}
    });
<?php
/**
 * EF2 Starter Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package EF2_Starter_Theme
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

require get_template_directory() . '/classes/theme-class.php';

$theme = new Ef2CustomTheme();

$theme->addNavMenus([
    'primary-menu' => esc_html__('Primary', 'ef2_custom'),
]);

$theme->addWidgetSidebar([
    'name'          => esc_html__('Sidebar', 'ef2_custom'),
    'id'            => 'sidebar-1',
    'description'   => esc_html__('Add widgets here.', 'ef2_custom'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
]);

$theme->addStyle('ef2_custom-style', get_stylesheet_uri(), [], _S_VERSION)
    ->addScript('ef2_custom-app', get_template_directory_uri() . '/js/app.js', [], _S_VERSION, true)
    ->addScript('ef2_custom-jquery', get_template_directory_uri() . '/js/jquery.min.js', [], _S_VERSION, true)
    ->addScript('ef2_custom-bundle', get_template_directory_uri() . '/js/bundle.js', [], _S_VERSION, true);

/**
 * SVG files
 */
add_filter('upload_mimes', function ($file_types) {
    $new_filetypes = [];
    $new_filetypes['svg'] = 'image/svg';

    return array_merge($file_types, $new_filetypes);
});

add_image_size('teaser', 450, 300, array( 'center', 'center' ) );
add_image_size('header', 730, 485, array( 'center', 'center' ) );
add_image_size('header-large', 1920, 425, array( 'center', 'center' ) );
add_image_size('image-with-text', 615, 560, array( 'center', 'center' ) );
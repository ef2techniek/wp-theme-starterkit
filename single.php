<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package EF2_Starter_Theme
 */

get_header();
?>

    <main id="page-wrapper" class="site-main">
        <?php get_template_part( 'template-parts/partials/partial', 'header-wrapper-simple' ); ?>

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->

<?php
get_footer();

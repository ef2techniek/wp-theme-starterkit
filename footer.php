<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EF2_Starter_Theme
 */

?>

<footer class="site-footer">
    <div class="inner-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 mb-3 mb-lg-0">
                    <a class="site-brand" rel="home" title="Home van <?= get_bloginfo('name'); ?>" href="<?= get_home_url(); ?>">
                        <?= wp_get_attachment_image(get_theme_mod('custom_logo'), 'full'); ?>
                    </a>
                </div>
                <div class="col-lg-2 offset-lg-2 mb-3 mb-lg-0">
                    <?php the_field('footer_column_left', 'option'); ?>
                </div>
                <div class="col-lg-2 offset-lg-1 mb-3 mb-lg-0">
                    <?php the_field('footer_column_center', 'option'); ?>
                    <div class="inner-footer__social-media">
                        <a href="<?php the_field('linkedin_url', 'option'); ?>">LinkedIn</a>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                    <?php the_field('footer_column_right', 'option'); ?>
                </div>
            </div>
        </div>
    </div>

    <aside id="copyright">
        <div class="container">
            <div class="copyright-links">
                <?php $copyrightUrl = get_field('copyright', 'option'); ?>
                <a href="<?= $copyrightUrl['url']; ?>"><?= $copyrightUrl['title']; ?></a>
            </div>
            <div class="site-info">
                <a href="https://ef2.nl" target="_blank" class="build-by">Gebouwd door
                    <span class="icon-logo-ef2">EF2</span>
                </a>
            </div><!-- .site-info -->
        </div>
    </aside>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

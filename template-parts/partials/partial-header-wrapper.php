<div class="header-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-7 col-sm-6">
                <div class="header-wrapper__content">
                    <h1><?php the_field('header_title'); ?></h1>
                    <div class="d-none d-lg-block">
                        <?php the_field('header_body'); ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-5 offset-md-1">
                <div class="header-wrapper__image">
                    <div class="header-image">
                        <?php the_post_thumbnail('header'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header-body d-lg-none mt-4 mb-8">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php the_field('header_body'); ?>
            </div>
        </div>
    </div>
</div>
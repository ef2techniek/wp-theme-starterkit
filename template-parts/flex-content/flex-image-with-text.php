<?php
$flipColumns = get_sub_field('reverse_order');
$hasBg = get_sub_field('has_bg');
?>

<div class="flex-image-with-text <?= ($hasBg) ? 'has-background section-spacing-padding' : 'section-spacing-margin' ?>">
    <div class="container">
        <div class="row <?= ($flipColumns ? 'flex-lg-row-reverse' : 'flex-lg-row'); ?>">
            <?php $image = get_sub_field('image');
            if (!empty($image)): ?>

                <div class="col-lg-5 <?= ($flipColumns ? 'offset-lg-1' : 'offset-lg-0'); ?>">
                    <img src="<?= esc_url($image['sizes']['image-with-text']); ?>" alt="<?= esc_attr($image['alt']); ?>" class="img-fluid" />
                </div>

            <?php endif; ?>

            <div class="col-lg-6 my-auto <?= ($flipColumns ? 'offset-lg-0' : 'offset-lg-1'); ?>">
                <?php the_sub_field('body'); ?>
            </div>
        </div>
    </div>
</div>
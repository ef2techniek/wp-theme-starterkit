<?php
if (have_rows('flex_content_wrapper')): ?>
    <div class="flex-content-wrapper">
        <?php
        while (have_rows('flex_content_wrapper')) : the_row();
            if (get_row_layout() == 'flex_text_block'):
                include('flex-text-block.php');

            elseif (get_row_layout() == 'flex_image_with_text'):
                include('flex-image-with-text.php');

            elseif (get_row_layout() == 'flex_teasers'):
                include('flex-teasers.php');

            elseif (get_row_layout() == 'flex_expertises'):
                include('flex-expertises.php');

            elseif (get_row_layout() == 'flex_steps'):
                include('flex-steps.php');

            endif;
        endwhile;
        ?>
    </div>
<?php
endif;
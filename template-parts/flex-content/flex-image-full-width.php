<div class="flex-image-full-width section-spacing-margin">
    <div class="container">
        <div class="row">
            <?php $image = get_sub_field('image');
            if (!empty($image)): ?>
                <div class="col-12">
                    <img src="<?= esc_url($image['sizes']['image-fullwidth']); ?>" alt="<?= esc_attr($image['alt']); ?>" class="img-fluid" />
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php
$hasBg = get_sub_field('has_bg');
$isCentered = get_sub_field('is_centered');
?>
<div class="flex-text-block <?= ($hasBg) ? 'has-background section-spacing-padding' : 'section-spacing-margin' ?>">
    <div class="container">
        <div class="row">
            <div class="content-wrapper col-lg-8 mx-auto <?= ($isCentered) ? 'text-center' : '' ?>">
                <?php the_sub_field('body'); ?>
            </div>
        </div>
    </div>
</div>
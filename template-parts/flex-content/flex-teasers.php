<?php
$hasBg = get_sub_field('has_bg');
?>
<div class="flex-teasers <?= ($hasBg) ? 'has-background section-spacing-padding' : 'section-spacing-margin' ?>">
    <div class="container">
        <div class="row">
            <div class="content-wrapper col-lg-8 mx-auto text-center">
                <?php the_sub_field('body'); ?>
            </div>
        </div>

        <div class="swiper-teasers mt-4 mt-lg-7">
            <div class="row swiper-wrapper">
                <?php
                $teasers = get_sub_field('teasers');
                if( $teasers ): ?>
                    <?php foreach( $teasers as $teaser ):
                        $permalink = get_permalink( $teaser->ID );
                        $title = get_the_title( $teaser->ID );
                        $featuredImage = get_post_thumbnail_id( $teaser->ID);
                        $teaserImage = wp_get_attachment_image_src($featuredImage, 'teaser');
                        $label = get_field('case_subject', $teaser->ID);
                        $teaserText = get_post_field('post_content', $teaser->ID );
                        ?>
                        <div class="teaser flex-teasers__teaser col-lg-4 swiper-slide">
                            <div class="teaser__image">
                                <img src="<?= esc_url($teaserImage[0]); ?>" alt="<?= $title; ?>" class="img-fluid" />
                            </div>
                            <div class="teaser__content">
                                <p class="label"><?= $label; ?></p>
                                <h3><?= $title; ?></h3>
                                <p class="teaser__text"><?= $teaserText; ?></p>
                                <a href="<?= $permalink; ?>" class="btn--primary">Bekijk de case</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
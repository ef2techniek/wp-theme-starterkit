<?php
    $bgImage = get_sub_field('background_image');
    if (!empty($bgImage)):
        $hasBg = true;
        $bgImage = 'style="background-image: url("' . esc_url($bgImage['sizes']['quote']) . ')"';
    endif;
?>

<div class="flex-quote section-spacing-margin">
    <div class="container">
        <div class="inner section-spacing-padding" <?= ($hasBg ? $bgImage : ''); ?>>
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto">
                    <?php the_sub_field('body'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
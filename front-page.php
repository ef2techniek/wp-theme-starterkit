<?php
/**
 * The template for displaying the front-page
 *
 */

get_header();
?>

    <main id="page-wrapper" class="site-main">
        <?php get_template_part( 'template-parts/partials/partial', 'header-wrapper' ); ?>

        <div class="entry-content">
            <?php include('template-parts/flex-content/flex-content.php'); ?>
        </div><!-- .entry-content -->
    </main><!-- #main -->

<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package EF2_Starter_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'ef2_custom' ); ?></a>

    <header id="masthead" class="site-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <nav id="site-navigation" class="main-navigation d-flex align-items-center">
                        <a class="site-brand" rel="home" title="Home van <?= get_bloginfo('name'); ?>" href="<?= get_home_url(); ?>">
                            <?= wp_get_attachment_image(get_theme_mod('custom_logo'), 'full'); ?>
                        </a>

                        <button id="menu-toggle" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false" aria-label="Hoofdmenu">
                            <span class="bars"></span>
                        </button>

                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location'    => 'primary-menu',
                                'items_wrap'        => '<h2 class="main-menu-title d-lg-none">Menu</h2><ul class="%2$s">%3$s</ul>'
                            )
                        );
                        ?>
                    </nav><!-- #site-navigation -->
                </div>
            </div>
        </div>
    </header><!-- #masthead -->
